import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { Cart } from '../models/cart.model';

@Injectable() 
export class CartService {

    carts: Cart[] = [];

    constructor() {
        const carts = JSON.parse(localStorage.getItem('carts') || '[]');
        if (carts.length === 0) {
            localStorage.setItem('carts', '[]');
        }
    }

    getAll(): Cart[] {
        return this.carts;
    }

    store(cart: Cart) {
        this.carts.push(cart);
        this.updateRecords();
    }

    update(cart: Cart) {
        const index = _.findIndex(this.carts, {id: cart.id});
        this.carts[index] = cart;
        this.updateRecords();
    }

    destroy(id: number) {
        const index = _.findIndex(this.carts, {id});
        this.carts.splice(index, 1);
        this.updateRecords();
    }

    private updateRecords() {
        localStorage.setItem('carts', JSON.stringify(this.carts));
    }

}