import { CartService } from "./cart.service";
import { ProductService } from "./products.service";
import { UserService } from "./users.service";
import { SMSService } from "./sms.service";


export const services: any[]  = [
    CartService,
    ProductService,
    SMSService,
    UserService
];

export * from "./cart.service";
export * from "./products.service";
export * from "./users.service";
export * from "./sms.service";

