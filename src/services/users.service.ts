import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { User } from '../models/user.model';

@Injectable()
export class UserService {

    users: User[] = [];

    constructor() {
        const users = JSON.parse(localStorage.getItem('users') || '[]');
        if (users.length === 0) {
            localStorage.setItem('users', '[]');
            this.users = [];
        } else {
          this.users = users;
        }
    }

    getAll(): User[] {
        return this.users;
    }

    store(user: User) {
        user.id = this.users.length + 1;
        this.users.push(user);
        this.updateRecords();
    }

    update(user: User) {
        const index = _.findIndex(this.users, {id: user.id});
        this.users[index] = user;
        this.updateRecords();
    }

    destroy(id: number) {
        const index = _.findIndex(this.users, {id});
        this.users.splice(index, 1);
        this.updateRecords();
    }

    login(username: string, password: string): boolean {
        const user = _.find(this.users, {username, password});
        console.log(user);
        if (typeof  user === 'undefined') {
            return false;
        }
        localStorage.setItem('login-user', JSON.stringify(user));
        return true;
    }

    getAuth() {
        const userString = localStorage.getItem('login-user');
        if(userString == null) return userString;
        return JSON.parse(userString);
    }

    logout(): void {
        localStorage.removeItem('login-user');
    }

    private updateRecords() {
        localStorage.setItem('users', JSON.stringify(this.users));
    }

}
