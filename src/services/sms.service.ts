import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class SMSService {

  endpoint = 'https://sms-test.bayadcenter.net/';

  constructor(private http: HttpClient) {}

  sendSMS(number, message = '') {
    const formData = new FormData();
    formData.append('number', number);
    formData.append('message', message);
    return this.http.post(`${this.endpoint}sms/push`, formData);
  }

}
