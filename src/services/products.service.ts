import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { Product } from '../models/product.model';
import {products} from "../mocks/product.mock";

@Injectable()
export class ProductService {

    products: Product[] = [];

    constructor() {
        const products$ = JSON.parse(localStorage.getItem('products') || '[]');
        if (products$.length === 0) {
            localStorage.setItem('products', JSON.stringify(products));
            this.products = products;
        } else {
          console.log(products$);
          this.products = products$;
        }

    }

    getAll(): Product[] {
        return this.products;
    }

    store(product: Product) {
        this.products.push(product);
        this.updateRecords();
    }

    update(product: Product) {
        const index = _.findIndex(this.products, {id: product.id});
        this.products[index] = product;
        this.updateRecords();
    }

    destroy(id: number) {
        const index = _.findIndex(this.products, {id});
        this.products.splice(index, 1);
        this.updateRecords();
    }

    private updateRecords() {
        localStorage.setItem('products', JSON.stringify(this.products));
    }

}
