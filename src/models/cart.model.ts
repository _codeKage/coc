export class Cart {
    constructor(
        public productId: number,
        public userId: number,
        public quantity: number,
        public id: number
    ) {}
}