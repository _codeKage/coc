export class User {
    constructor(
       public id: number=null,
       public  firstname: string =null,
       public lastname: string =null,
       public email: string =null,
       public contact: string =null,
       public birthdate: string =null,
       public username: string =null,
       public password: string =null,
       public address: string = null
    ) {}
}