import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CartPage } from '../pages/cart/cart';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { ProductListPage } from '../pages/product-list/product-list';
import { ProfilePage } from '../pages/profile/profile';

import * as fromServices from '../services';
import { RegistrationPage } from '../pages/registration/registration';
import { Registration_2Page } from '../pages/registration-2/registration-2';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CartPage,
    ProductDetailsPage,
    ProductListPage,
    ProfilePage,
    RegistrationPage,
    Registration_2Page
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CartPage,
    ProductDetailsPage,
    ProductListPage,
    ProfilePage,
    RegistrationPage,
    Registration_2Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ...fromServices.services,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
