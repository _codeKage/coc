import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { UserService } from '../../services/users.service';
import { ProductListPage } from '../product-list/product-list';
import { RegistrationPage } from '../registration/registration';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  username = '';
  password ='';

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              private userService: UserService) {
    console.log('here');
    if (this.userService.getAuth() != null) {
      this.navCtrl.setRoot(ProductListPage);
    }
  }

  ionViewDidLoad(){
    console.log('here');

  }

  login() {
    const check = this.userService.login(this.username, this.password);
    if(check) {
      this.navCtrl.setRoot(ProductListPage);
    } else {
      const alert = this.alertCtrl.create({
        title: 'Warning',
        subTitle: 'Wrong username or password!'
      });
      alert.present();
    }
  }

  navigateToSignUp() {
    this.navCtrl.push(RegistrationPage);
  }

}
