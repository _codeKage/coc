import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Registration_2Page } from '../registration-2/registration-2';
import { User } from '../../models/user.model';

/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {

  user: User;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user = new User();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }

  nextPage(): void {
    this.navCtrl.push(Registration_2Page, {
        'user': this.user
    });
  }

}
