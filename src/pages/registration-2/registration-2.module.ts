import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Registration_2Page } from './registration-2';

@NgModule({
  declarations: [
    Registration_2Page,
  ],
  imports: [
    IonicPageModule.forChild(Registration_2Page),
  ],
})
export class Registration_2PageModule {}
