import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {User} from "../../models/user.model";
import {SMSService} from "../../services/sms.service";
import {UserService} from "../../services";
import {HomePage} from "../home/home";
/**
 * Generated class for the Registration_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration-2',
  templateUrl: 'registration-2.html',
})
export class Registration_2Page {
  user: User;
  password:string;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private smsService: SMSService,
              private userService: UserService) {
               this.user = this.navParams.get('user');
               console.log(this.password);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Registration_2Page');
  }

  register(): void {
    if(!this.validatePassword()){
      this.alertCtrl.create({
        title: 'Message',
        subTitle: 'Password does not match',
        buttons: ['OK']
      }).present();
    }else{
      this.sendMessage();
    }
  }

  validatePassword(): boolean {
    return this.password == this.user.password;
  }

  private sendMessage() {
    const code = this.generateRandomCode();
    const loader = this.loadingCtrl.create({
      content: 'Please wait..'
    });
    loader.present();
    this.smsService.sendSMS(this.user.contact,
      `Hello ${this.user.firstname}, ${this.user.lastname}, Thank you for your registration on our App,\n Here is your Confirmation code: ${code}.`).subscribe(res => {
       loader.dismiss();
       this.confirmCodeAlert(code);
    });
  }

  private confirmCodeAlert(code) {

      const prompt = this.alertCtrl.create({
        title: 'Registration',
        message: "Enter your Confirmation Code:",
        enableBackdropDismiss: false,
        inputs: [
          {
            name: 'code',
            placeholder: 'Code'
          },
        ],
        buttons: [
          {
            text: 'Submit',
            handler: data => {
              if (data.code === code) {
                this.userService.store(this.user);
                this.showSuccessAlert();
              }
              else {
                this.confirmCodeAlert(code);
              }
            }
          }
        ]
      });
      prompt.present();
  }

  private generateRandomCode(): string {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
  }

  private showSuccessAlert() {
    const successAlert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'Registered Successfully',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.setRoot(HomePage);
          }
        }
      ]
    });
    successAlert.present();
  }

}
