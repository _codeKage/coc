import { Component } from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ProductService} from "../../services";
import {Product} from "../../models/product.model";
import {ProductDetailsPage} from "../product-details/product-details";

/**
 * Generated class for the ProductListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {

  products: Product[] = [];
  productsOriginal = [];
  search = '';

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public productService: ProductService,
              private alertCtrl: AlertController,
              private actionSheetCtrl: ActionSheetController) {
    this.products = this.productService.getAll();
    console.log(this.products);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductListPage');
    this.products = this.productService.getAll();
    this.productsOriginal = this.products;
    console.log(this.products);
  }

  searchProducts() {
    console.log(this.productsOriginal);
    this.products = this.productsOriginal.filter(product => product.name.toLowerCase().includes(this.search.toLowerCase()));
  }

  productClicked(product: Product) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Modify your album',
      buttons: [
        {
          text: 'Cancel',
          role: 'destructive',
          handler: () => {

          }
        },
        {
          text: 'View Details',
          handler: () => {
            this.navCtrl.push(ProductDetailsPage,{
              'product': product,

            });
          }
        }
         ]
    });

    actionSheet.present();
  }


}
