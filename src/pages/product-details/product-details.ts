import { Component } from '@angular/core';
import {AlertController, Config, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Product} from "../../models/product.model";
import {CartService, SMSService, UserService} from "../../services";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../../models/user.model";

/**
 * Generated class for the ProductDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {
  product: Product;
  quantity:number;
  constructor(public navCtrl: NavController, public navParams: NavParams, private cartService:CartService, public alertCtrl:AlertController,
              private http: HttpClient,
              private  smsService:SMSService,
              private userService: UserService) {
    this.product = navParams.get('product');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailsPage');
  }

  motherFuckingPurchase(): void {
    let prompt = this.alertCtrl.create({
      title: 'Purchase',
      message: 'Mode of Payment',
      inputs : [
        {
          type:'radio',
          label:'Cash on Delivery',
          value:'COD',
          name:'remark',
        },
        {
          type:'radio',
          label:'Bayad Center',
          value:'BC',
          name:'remark'
        },
        {
          type: 'radio',
          label: 'Bank Payment',
          value: 'BP',
          name: 'remark'
        }
      ],
      buttons : [
        {
          text: "Cancel",
          role: 'Cancel'
        },
        {
          text: "Ok",
          handler: (data) => {
            let postData = new FormData();
            let cheeseburger = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
            postData.append('amount', this.product.price.toString());
            postData.append('txnid', cheeseburger);
            postData.append('callback_url','https://google.com');
            postData.append('name',cheeseburger);
            let modeOfPayment = data;
            let headers = new HttpHeaders();
            headers.append('X-MultiPay-Token','9ed98c02548aefad00eee6e87ea5bf2cc2c2cd9a');
            headers.append('X-MultiPay-Code','COC');
            console.log(headers);
              this.http.post('https://pgi-api-test.bayadcenter.net/api/v1/transactions/generate',postData, {
                headers: new HttpHeaders({
                  'X-MultiPay-Token' : '9ed98c02548aefad00eee6e87ea5bf2cc2c2cd9a',
                  'X-MultiPay-Code' : 'COC'
                })
              }).subscribe(res=>{
              console.log(res);
              this.alertCtrl.create({
                title: 'Success',
                subTitle: 'Transaction Code: '+ cheeseburger,
                buttons: ['Ok']
              }).present();
                let x: User;
                x = this.userService.getAuth();
                console.log(x.contact);

                this.smsService.sendSMS(x.contact,
                  `Transaction Code  for `+ this.product.name  + ` : `+cheeseburger ).subscribe(res => {
                });
            });
          }
        }]});
    prompt.present();
  }
}
