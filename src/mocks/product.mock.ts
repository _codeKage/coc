import {Product} from "../models/product.model";

export const products: Product[] = <any>[
  {
    id: 1,
    name: 'Away Jersey',
    price: 500,
    quantity: 20,
    description: 'High School Away Jersey',
    type: 'Jersey',
    brand: 'NSNU'
  },
  {
    id: 2,
    name: 'NU Lady Bulldogs Volleyball Jersey',
    price: 450,
    quantity: 20,
    description: 'Reyes 20 NU Lady Bulldogs Volleyball Jersey',
    type: 'Jersey',
    brand: 'NU'
  },
  {
    id: 3,
    name: 'NU Men\'s Basketball Jersey ',
    price: 700,
    quantity: 20,
    description: 'National University Men\'s Basketball Jersey',
    type: 'Jersey',
    brand: 'NU'
  },
  {
    id: 4,
    name: 'CCS Lace',
    price: 200,
    quantity: 20,
    description: 'NU CCS ID Lace',
    type: 'Lace',
    brand: 'NU'
  },
  {
    id: 5,
    name: 'Architecture Lace',
    price: 200,
    quantity: 20,
    description: 'NU Architecture Lace',
    type: 'Lace',
    brand: 'NU'
  }
];
